package com.example.mightystrong.locationservicesexample;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Evan Anger on 7/6/14.
 */
@SuppressLint("ValidFragment")
public class LocationFragment extends Fragment {

    Button btnGetLocation;
    TextView tvCurrentLocation;
    View.OnClickListener mOnClickListener;

    public LocationFragment(View.OnClickListener onClickListener) {
        mOnClickListener = onClickListener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        btnGetLocation = (Button)rootView.findViewById(R.id.btnGetLocation);
        btnGetLocation.setOnClickListener(mOnClickListener);
        tvCurrentLocation = (TextView)rootView.findViewById(R.id.tvCurrentLocation);
        return rootView;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    public void updateLocation(Location location) {
        tvCurrentLocation.setText(String.format("{lat:%f, long:%f}", location.getLatitude(), location.getLongitude()));
    }
}