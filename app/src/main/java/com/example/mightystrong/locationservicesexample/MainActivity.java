package com.example.mightystrong.locationservicesexample;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;


public class MainActivity extends Activity {

    private static String MOCK_LOCATION_PROVIDER = "MockLocation";
    private static String MOCK_LOCATION_BROADCAST_ACTION = "com.example.mightystrong.locationservicesexample.SEND";
    private String mCurrentLocationProvider;
    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Log.d(MainActivity.this.getClass().getSimpleName(), "Get Location");
            mLocationManager.requestLocationUpdates(mCurrentLocationProvider, 0, 0, mLocationListener);
        }
    };

    LocationManager mLocationManager;
    LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            Log.d(this.getClass().getSimpleName(), String.format("Update location from %s {lat:%f, long:%f}",
                    location.getProvider(),
                    location.getLatitude(),
                    location.getLongitude()));
            mLocationFragment.updateLocation(location);
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    };

    BroadcastReceiver mBroadcastReceiver;
    LocationFragment mLocationFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d(context.getClass().getSimpleName(), "Received... something");
                Location location = new Location(MOCK_LOCATION_PROVIDER);
                location.setLatitude(Double.valueOf(intent.getStringExtra("Latitude")));
                location.setLongitude(Double.valueOf(intent.getStringExtra("Longitude")));
                location.setAltitude(Double.valueOf(intent.getStringExtra("Altitude")));
                location.setTime(System.currentTimeMillis());
                mLocationManager.setTestProviderLocation(MOCK_LOCATION_PROVIDER, location);
            }
        };
        mLocationManager = (LocationManager)MainActivity.this.getSystemService(Context.LOCATION_SERVICE);
        mLocationManager.removeTestProvider(MOCK_LOCATION_PROVIDER);
        mLocationManager.addTestProvider(MOCK_LOCATION_PROVIDER, false, false, false, false, true, true, true, 0, 5);
        mLocationManager.setTestProviderEnabled(MOCK_LOCATION_PROVIDER, true);
        mCurrentLocationProvider = LocationManager.GPS_PROVIDER;
        setContentView(R.layout.activity_main);
        mLocationFragment =  new LocationFragment(mOnClickListener);
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, mLocationFragment)
                    .commit();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(MOCK_LOCATION_BROADCAST_ACTION);
        this.registerReceiver(mBroadcastReceiver, intentFilter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        this.unregisterReceiver(mBroadcastReceiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public void restartLocationManager() {
        mLocationManager.removeUpdates(mLocationListener);
        mLocationManager.requestLocationUpdates(mCurrentLocationProvider, 0, 0, mLocationListener);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_gps) {
            mCurrentLocationProvider = LocationManager.GPS_PROVIDER;
            restartLocationManager();
            return true;
        } else if(id == R.id.action_passive) {
            mCurrentLocationProvider = LocationManager.PASSIVE_PROVIDER;
            restartLocationManager();
            return true;
        } else if(id == R.id.action_network) {
            mCurrentLocationProvider = LocationManager.NETWORK_PROVIDER;
            restartLocationManager();
            return true;
        } else if(id == R.id.action_mock) {
            mCurrentLocationProvider = MOCK_LOCATION_PROVIDER;
            restartLocationManager();
        }
        return super.onOptionsItemSelected(item);
    }

}
